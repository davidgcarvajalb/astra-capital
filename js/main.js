// Tooltips for Social Links
$('.tooltip-social').tooltip({
  selector: "a[data-toggle=tooltip]"
})

// Flexslider
$(document).ready(function($) {
	$('#main-slider').flexslider({
		animation: "fade",
		slideshowSpeed: 3500,
		controlNav: false,
		directionNav: false
	});
  $('.monto').autoNumeric('init');  
});

// Owl Carousel
$(document).ready(function($) {
      $("#owl-example").owlCarousel();
});

// Mailchimp Newsletter
$(document).ready(function() {
	$('#invite').ketchup().submit(function() {
	if ($(this).ketchup('isValid')) {
	var action = $(this).attr('action');
	$.ajax({
	url: action,
	type: 'POST',
	data: {
	email: $('#address').val()
	},
	success: function(data){
	$('#result').html(data).css('color', 'white');
	},
	error: function() {
	$('#result').html('Sorry, an error occurred.').css('color', 'white');
	}
	});
	}
	return false;
	});
});

// Calculadora
$(document).ready(function($) {
   $("form.calculadora").submit(function(){
    descuento = new Array(0.00000000000000000	,0.02380000000000000	,0.05866530318216530	,0.07906019812000590	,0.09353060636433050	,0.10475472699543500	,0.11392550130217100	,0.12167928049748200	,0.12839590954649600	,0.13432039624001200	,0.13962003017760000	,0.14441413222175800	,0.15045826687247500	,0.15281695268031500	,0.15654458367964800	,0.16001492511544100	,0.16326121272866100	,0.16631063120602800	,0.16918569942217700	,0.17190528065207200	,0.17448533335976600	,0.17693947861748800	,0.17927943540392300	,0.18151535906123600	,0.18023498192337200	,0.18570945399087000	,0.18768225586248100	,0.18958059436001800	,0.19140988686181300	,0.19317498024832000	,0.19488022829760600	,0.19652955638560300	,0.19812651591082600	,0.19967433034176400	,0.20117593438819300	,0.20263400749291700	,0.20443656109482300	,0.20542917100600400	,0.20677058383423700	,0.20807715080032100	,0.20935063654193100	,0.21059267495522700	,0.21180478179965300	,0.21298836581938600	,0.21414473858608900	,0.21527512323544700	,0.21638066224340100	,0.21746242436601600	,0.21908277244460500	,0.21955856099496500	,0.22057475717303600	,0.22157082932603400	,0.22254755904464600	,0.22350568325167200	,0.22444589754218300	,0.22536885921719300	,0.22627519004397800	,0.22716547877207800	,0.22804028343048500	,0.22890013342845800	,0.23029119391475100);
    capital = $('.monto').autoNumeric('get');
   	plazo = $('.plazo',this).val();
    tir = descuento[plazo];
    t_mes_tir = 1 + tir;
    t_mes_val_presente = (1/12)
    t_mes_equivalente = Math.pow(t_mes_tir, t_mes_val_presente) - 1;
    mensualidad = capital/((1 - Math.pow((1+t_mes_equivalente), - plazo))/t_mes_equivalente) * (Math.pow((1+t_mes_equivalente), 0));
    total_capital_intereses = mensualidad * plazo;    
    tir_con_formato = (tir * 100).toFixed(2) + ' %';
    t_mes_equivalente_con_formato = (t_mes_equivalente * 100).toFixed(2) + ' %';
    mensualidad_con_formato = '$ ' + addCommas(mensualidad.toFixed(0));
    total_capital_intereses_con_formato = '$ ' + addCommas(total_capital_intereses.toFixed(0));
    $('.tir').val(tir_con_formato);
    $('.flujomes').val(mensualidad_con_formato);
    $('.retorno').val(t_mes_equivalente_con_formato);
    $('.totalcapitalinteres').val(total_capital_intereses_con_formato);
   	return false;
   });
});
function addCommas(nStr)
{
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + '.' + '$2');
  }
  return x1 + x2;
}
