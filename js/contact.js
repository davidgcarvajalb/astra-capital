var messageDelay = 2000;  // How long to display status messages (in milliseconds)

   $(function(){
      $('.order-wrap .apply-form').submit(function(e){
        var thisForm = $(this);
        var fn = $("[name='firstname']",thisForm).val();
        var ln = $("[name='lastname']",thisForm).val();
        var em = $("[name='email']",thisForm).val();
        var ph = $("[name='phone']",thisForm).val();
        var ct = $("[name='booklist']",thisForm).find(":selected").text();
        var hn = $("[name='honey']",thisForm).val();
        //Prevent the default form action
        e.preventDefault();
        //Hide the form
        $(this).fadeIn(function(){
          //Display the "loading" message
          $("#loading").fadeIn(function(){
            //Post the form to the send script
            jQuery.get('php/contact.php', 'fn='+fn+'&ln='+ln+'&em='+em+'&ph='+ph+'&ct='+ct+'&hn='+hn, function (response) {
              //alert("Hemos recibido su información en Astra Capital, en breve uno de nuestros representantes se comunicará con Usted. Gracias.");
              //Hide the "loading" message
              $("#loading").fadeOut(function(){
                //Display the "success" message
                $("#success").text("Cita solicitada correctamente").fadeIn().delay(messageDelay);
              });              
            });            
          });
        });
      })
    });