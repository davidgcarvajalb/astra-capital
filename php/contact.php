<?php
if (isset($_REQUEST['em']) AND $_REQUEST['hn'] == ""){
  //open the database
  $db = new PDO('sqlite:astra_Capital.sqlite');

  //create the database
  $db->exec("CREATE TABLE Leeds (Id INTEGER PRIMARY KEY, Nombre TEXT, Apellido TEXT, Correo TEXT, Telefono TEXT, Ciudad TEXT, Fecha TEXT)");    
	
	$fn = $_REQUEST['fn'];
	$ln = $_REQUEST['ln'];
	$em = $_REQUEST['em'];
	$ph = $_REQUEST['ph'];
	$ct = $_REQUEST['ct'];
  $dt = date('Y-m-d H:i:s');
	$query ="INSERT INTO Leeds (Nombre, Apellido, Correo, Telefono, Ciudad, Fecha) VALUES ('$fn','$ln','$em','$ph','$ct', '$dt');";
  //insert some data...
  $res = $db->exec($query);
  $db = NULL;
  $mensaje = "";
  if ($res) {
    $flagdb = TRUE;
    //Mail cliente
    $to = $em;
    $from = "contacto@astracapital.co";
    $subject = 'Gracias por Registrarse en Astra Capital ';
    $headers = "From: " . strip_tags($from) . "\r\n";
    $headers .= "Reply-To: ". strip_tags($from) . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    $message = get_html_mail($fn,$ln);

    //Mail admin
    $to_admin = "gamorocho@astracapital.co,contacto@astracapital.co";
    $from_admin = "contacto@astracapital.co";
    $subject_admin = 'Nuevo Leed en Astra Capital';
    $headers_admin = "From: " . strip_tags($from_admin) . "\r\n";
    $headers_admin .= "Reply-To: ". strip_tags($from_admin) . "\r\n";
    $headers_admin .= "MIME-Version: 1.0\r\n";
    $headers_admin .= "Content-Type: text/html; charset=ISO-8859-1\r\n";    
    $message_admin = get_html_admin($fn,$ln,$em,$ph,$ct,$dt);
    
    mail($to_admin, $subject_admin, $message_admin, $headers_admin);
    mail($to, $subject, $message, $headers);
  }
}
function get_html_mail($fn = "",$ln = ""){
  $mail= 
' <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1" />
     <title>Instant : Business Email Template</title>

     <style>
     @media only screen and (max-width: 640px)  {
      table[class=table600con] img { max-width: 100% !important; width: 100% !important; margin: auto !important; height: auto !important}

      div[class=wrapper] { width: 100% !important; }
      table[class=table600con], table[class=table600] { width: 80% !important; }
      td[class=fw_full], td[class=fw_half], td[class=grid_block] { width: 100% !important; display: block !important; text-align: center !important;}

      td[class=spacer], table[class=line] { height: 0 !important; width: 0 !important; display: none !important;}

      td[class=grid_block] { width: 100% !important; text-align: center !important; padding: 15px 0 !important;}
      td[class=grid_block_nopad] { width: 100% !important; padding: 0 !important; display: block !important; text-align: center !important;}
      td[class=grid_block] table { margin: auto !important;}

      tr[class=scaleimage] img { width: auto !important}

      td[class=boxpadding] { width: 7% !important; }
      td[class=boxcontent] { width: 86% !important; }

      div[class=divider] { width: 70%; height: 1px; border-bottom: 1px solid #aaaaaa; margin: 25px auto 0;}
    }

    a { text-decoration: none; }
    .ReadMsgBody { width: 100%; }
    .ExternalClass { width: 100%; }
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
    #outlook a { padding: 0; }
    span.yshortcuts { color:#000; background-color:none; border:none;}
    span.yshortcuts:hover,
    span.yshortcuts:active,
    span.yshortcuts:focus {color:#000; background-color:none; border:none;}
    body[class=body] { min-width: 0 !important; }
    
     </style>
  </head>

  <body class="body" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;font-family: sans-serif;font-size: 10pt;color: #888888;mso-line-height-rule: exactly;line-height: 1.3em;width: 100%; min-width: 600px;">
     <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center" style="padding: 0;margin: 0;border-collapse: collapse;">
      <tbody>
      <!-- START -- HEAD LINK -->
        <tr>
          <td width="100%" bgcolor="#383838" align="center" class="full_width" style="border-collapse: collapse;">
            <div class="wrapper" style="width: 600px;margin: auto;">
              <table border="0" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">
                <tr>
                    <td width="280" class="grid_block" style="padding-top: 35px;padding-bottom: 30px;border-collapse: collapse;" align="left" valign="middle">
                      <img src="http://astracapital.co/images/logo_astra_elite.png" alt="" style="display: block;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;">
                    </td>
                    <td width="20" class="spacer" style="border-collapse: collapse;"></td>
                    <td width="280" class="grid_block" style="padding-top: 35px;padding-bottom: 30px;font-weight: bold;color: #FFFFFF;border-collapse: collapse;font-family: sans-serif;" align="right" valign="middle">
                      Llámanos:<br><br><span style="color: #98c029">+57 320 306 5437<br>+57 312 431 6565</span>
                    </td>
                    <td width="20" class="spacer" style="border-collapse: collapse;"></td>
                </tr>
            </table>
            </div>
          </td>
        </tr>
        <tr>
          <td width="100%" height="5" bgcolor="#98c029" align="center" class="full_width" style="border-collapse: collapse;">
          </td>
        </tr>
        <!-- END -- HEAD LINK -->

        <!-- START -- HEADER -->
        <tr>
          <td width="100%" bgcolor="#CCCBC9" align="center" class="full_width" style="border-collapse: collapse;">
            <div class="wrapper" style="width: 600px;margin: auto;">
              <table border="0" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">
                <tr>
                    <td height="2" style="border-collapse: collapse;"></td>
                </tr>
            </table>
            </div>
          </td>
        </tr>
        <!-- END -- HEADER -->

        <!-- **************** START LAYOUT ZONE **************** -->
<!-- START BLOCK - Content 1 Column (with button) -->
<tr>
  <td width="100%" bgcolor="#F0F0F0" align="center" class="full_width" style="border-collapse: collapse;">
    <div class="wrapper" style="width: 600px;margin: auto;">
      <table border="0" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">
        <tr>
            <td width="20" class="boxpadding" style="border-collapse: collapse;"></td>
            <td width="260" class="boxcontent" align="center" style="border-collapse: collapse; padding-top: 30px; padding-bottom: 30px;font-family: sans-serif;text-align:left;">
              <h2 style="color: #98c029; font-weight: bold; font-size: 16pt; margin: 0; padding: 0; margin-top: 20px;">Apreciado '.$fn.' '.$ln.':</h2>
              <p style="margin: 1em 0;line-height:20px;text-align: justify;color:#888888;">
                Gracias por suscribirse a Astra Capital. Hemos recibido su información y pronto uno de nuestros representantes se comunicará con Usted para agendar una cita y explicarle en detalle los beneficios y las condiciones de los títulos pagaré libranza.<br><br>

                Si desea comunicarse con nosotros, puede escribirnos a <a style="color: #888888; font-weight: bold;text-decoration:underline;" href="mailto:info@astracapital.co">info@astracapital.co</a> o llamar en Colombia a los teléfonos <strong>(+57) 320 306 5437</strong> o <strong>(+57) 312 431 6565</strong>.<br><br>

                Gracias por su interés, lo invitamos a visitar nuestras páginas: <a style="color: #888888; font-weight: bold;text-decoration:underline;" href="http://www.astracapital.co">Astra Capital</a> y <a style="color: #888888; font-weight: bold;text-decoration:underline;" href="http://elite.net.co">Elite International Americas</a>.<br><br>

                Cordialmente,
              </p>
            </td>
            <td width="20" class="boxpadding" style="border-collapse: collapse;"></td>
        </tr>
    </table>
    </div>
  </td>
</tr>
<!-- END BLOCK - Content 1 Column (with button) -->
<!-- **************** END LAYOUT ZONE **************** -->

        <!-- START -- FOOTER LINK -->
        <tr>
          <td width="100%" bgcolor="#383838" align="center" class="full_width" style="border-collapse: collapse;">
            <div class="wrapper" style="width: 600px;margin: auto;">
              <table border="0" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">
                <tr>
                    <td width="600" class="fw_full" style="padding-top: 15px;padding-bottom: 20px;color: #FFFFFF;border-collapse: collapse;font-family: sans-serif; line-height:22px;" align="center">
                      Elite International Americas S.A.S<br>
                      Carrera 11A #93-67 Oficina 504,<br>
                      Bogotá, Colombia
                    </td>
                </tr>
            </table>
            </div>
          </td>
        </tr>
        <!-- END -- FOOTER LINK -->

      </tbody>
     </table>
  </body>';
  return $mail;
}

function get_html_admin($fn,$ln,$em,$ph,$ct,$dt){
  $mail= ' 
  <head>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
     <meta name="viewport" content="width=device-width, initial-scale=1" />
     <title>Instant : Business Email Template</title>

     <style>
     @media only screen and (max-width: 640px)  {
      table[class=table600con] img { max-width: 100% !important; width: 100% !important; margin: auto !important; height: auto !important}

      div[class=wrapper] { width: 100% !important; }
      table[class=table600con], table[class=table600] { width: 80% !important; }
      td[class=fw_full], td[class=fw_half], td[class=grid_block] { width: 100% !important; display: block !important; text-align: center !important;}

      td[class=spacer], table[class=line] { height: 0 !important; width: 0 !important; display: none !important;}

      td[class=grid_block] { width: 100% !important; text-align: center !important; padding: 15px 0 !important;}
      td[class=grid_block_nopad] { width: 100% !important; padding: 0 !important; display: block !important; text-align: center !important;}
      td[class=grid_block] table { margin: auto !important;}

      tr[class=scaleimage] img { width: auto !important}

      td[class=boxpadding] { width: 7% !important; }
      td[class=boxcontent] { width: 86% !important; }

      div[class=divider] { width: 70%; height: 1px; border-bottom: 1px solid #aaaaaa; margin: 25px auto 0;}
    }

    a { text-decoration: none; }
    .ReadMsgBody { width: 100%; }
    .ExternalClass { width: 100%; }
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
    #outlook a { padding: 0; }
    span.yshortcuts { color:#000; background-color:none; border:none;}
    span.yshortcuts:hover,
    span.yshortcuts:active,
    span.yshortcuts:focus {color:#000; background-color:none; border:none;}
    body[class=body] { min-width: 0 !important; }
    
     </style>
  </head>

  <body class="body" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;font-family: sans-serif;font-size: 10pt;color: #888888;mso-line-height-rule: exactly;line-height: 1.3em;width: 100%; min-width: 600px;">
     <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" align="center" style="padding: 0;margin: 0;border-collapse: collapse;">
      <tbody>
      <!-- START -- HEAD LINK -->
        <tr>
          <td width="100%" bgcolor="#383838" align="center" class="full_width" style="border-collapse: collapse;">
            <div class="wrapper" style="width: 600px;margin: auto;">
              <table border="0" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">
                <tr>
                    <td width="280" class="grid_block" style="padding-top: 35px;padding-bottom: 30px;border-collapse: collapse;" align="left" valign="middle">
                      <img src="http://astracapital.co/images/logo_astra_elite.png" alt="" style="display: block;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;">
                    </td>
                    <td width="20" class="spacer" style="border-collapse: collapse;"></td>
                    <td width="280" class="grid_block" style="padding-top: 35px;padding-bottom: 30px;font-weight: bold;color: #FFFFFF;border-collapse: collapse;font-family: sans-serif;" align="right" valign="middle">
                      Llámanos:<br><br><span style="color: #98c029">+57 320 306 5437<br>+57 312 431 6565</span>
                    </td>
                    <td width="20" class="spacer" style="border-collapse: collapse;"></td>
                </tr>
            </table>
            </div>
          </td>
        </tr>
        <tr>
          <td width="100%" height="5" bgcolor="#98c029" align="center" class="full_width" style="border-collapse: collapse;">
          </td>
        </tr>
        <!-- END -- HEAD LINK -->

        <!-- START -- HEADER -->
        <tr>
          <td width="100%" bgcolor="#CCCBC9" align="center" class="full_width" style="border-collapse: collapse;">
            <div class="wrapper" style="width: 600px;margin: auto;">
              <table border="0" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">
                <tr>
                    <td height="2" style="border-collapse: collapse;"></td>
                </tr>
            </table>
            </div>
          </td>
        </tr>
        <!-- END -- HEADER -->

        <!-- **************** START LAYOUT ZONE **************** -->
<!-- START BLOCK - Content 1 Column (with button) -->
<tr>
  <td width="100%" bgcolor="#F0F0F0" align="center" class="full_width" style="border-collapse: collapse;">
    <div class="wrapper" style="width: 600px;margin: auto;">
      <table border="0" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">
        <tr>
            <td width="20" class="boxpadding" style="border-collapse: collapse;"></td>
            <td width="260" class="boxcontent" align="center" style="border-collapse: collapse; padding-top: 30px; padding-bottom: 30px;font-family: sans-serif;text-align:left;">
              <h2 style="color: #98c029; font-weight: bold; font-size: 16pt; margin: 0; padding: 0; margin-top: 20px;">Se ha registrado un nuevo usuario en Astracapital.co:</h2>
              <p style="margin: 1em 0;line-height:20px;text-align: justify;color:#888888;">
                - Nombre: '.$fn.' <br>
                - Apellido: '.$ln.' <br>
                - Correo: '.$em.' <br>
                - Teléfono: '.$ph.' <br>
                - Ciudad: '.$ct.' <br>
                - Fecha del registro: '.$dt.' <br><br>
                Para descargar la lista de leeds por favor ingrese a: <a href="http://astracapital.co/php/reporte.php?user=gamorocho&pass=gamorocho2015.*">Descargar reporte</a><br>
                Cordialmente,
              </p>
            </td>
            <td width="20" class="boxpadding" style="border-collapse: collapse;"></td>
        </tr>
    </table>
    </div>
  </td>
</tr>
<!-- END BLOCK - Content 1 Column (with button) -->
<!-- **************** END LAYOUT ZONE **************** -->

        <!-- START -- FOOTER LINK -->
        <tr>
          <td width="100%" bgcolor="#383838" align="center" class="full_width" style="border-collapse: collapse;">
            <div class="wrapper" style="width: 600px;margin: auto;">
              <table border="0" cellpadding="0" cellspacing="0" width="600" class="table600con" style="border-collapse: collapse;">
                <tr>
                    <td width="600" class="fw_full" style="padding-top: 15px;padding-bottom: 20px;color: #FFFFFF;border-collapse: collapse;font-family: sans-serif; line-height:22px;" align="center">
                      Elite International Americas S.A.S<br>
                      Carrera 11A #93-67 Oficina 504,<br>
                      Bogotá, Colombia
                    </td>
                </tr>
            </table>
            </div>
          </td>
        </tr>
        <!-- END -- FOOTER LINK -->

      </tbody>
     </table>
  </body>';  
  return $mail;
}
?>

